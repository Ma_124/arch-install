#!/bin/bash
set -euo pipefail

log() {
    printf "\033[32m%s\033[39m\n" "$*"
}

log "User:    ${NEWUSER:=core}"
log "Comment: ${USERCOMMENT:="$NEWUSER"}"
log "Shell:   ${NEWSHELL:=zsh}"

log
log "Add user $NEWUSER"
useradd -c "$USERCOMMENT" --create-home --shell "$NEWSHELL" -u 1000 -U "$NEWUSER"
passwd "$NEWUSER"

log
log "Lock root"
passwd --lock root

log
log "Enable DHCP"
systemctl --enable now

log
log "Detect required CPU microcode"
if   grep AuthenticAMD < /proc/cpuinfo > /dev/null; then
    pacman -S amd-ucode
elif grep GenuineIntel < /proc/cpuinfo > /dev/null; then
    pacman -S intel-ucode
else
    echo "\033[31mNeither AMD nor Intel CPU, no microcode installed.\033[39m"
fi

