#!/bin/bash
set -euo pipefail

rm -rf public
mkdir public
cp -- *.sh public

cat << EOF > public/download
#!/bin/sh

curl $CI_SERVER_URL/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/-/archive/$CI_COMMIT_SHA/arch-install.tar.gz | tar xvz --strip-components=1
EOF

cat << EOF > public/index.html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <style>
            $(cat .gitlab/public/style.css)
            /*
            $(cat .gitlab/public/highlight/LICENSE)
            */
            $(cat .gitlab/public/highlight/theme.css)
        </style>
        <title>Ma_124's Arch Installer</title>
    </head>
    <body>
        <h1>Ma_124's Arch Installer</h1>
        My personal arch installer.

        Start with <a href="#installsh"><code>./install.sh</code></a>.

        <pre><code class="language-bash">curl "$CI_PAGES_URL/download" | bash</code></pre>
EOF

for f in install{,-cli}.sh; do
    cat << EOF >> public/index.html
<h2 id="$(tr -d '.' <<< "$f")"><code>$f</code></h2>
<pre><code class="language-bash">$(
        sed -E 's/</\&lt;/g ; s/>/\&gt;/g' < "$f" | \
        sed -E 's|(cat \&lt;\&lt; EOR \&gt; .*$)|\1</code><code class="language-bash">|g'
    )</code></pre>
EOF
done

cat << EOF >> public/index.html
        <script>
            $(cat .gitlab/public/highlight/highlight.min.js)
            document.addEventListener('DOMContentLoaded', (event) => {
                document.querySelectorAll('pre code').forEach((el) => {
                    hljs.highlightElement(el);
                    el.innerHTML = el.innerHTML.replace("EOR", '<span class="sep">EOR</span>');
                });
            });
        </script>
    </body>
</html>
EOF
