#!/bin/bash
set -euo pipefail

log() {
    printf "\033[32m%s\033[39m\n" "$*"
}

log "Mirror Countries: ${COUNTRIES:=Germany,Switzerland,France,United States}"
log "Timezone:         ${TIMEZONE:=Europe/Berlin}"
log "Locales:          ${LOCALES:=en_GB,en_US,de_DE}"
log "Keyboard Layout:  ${KEY_LAYOUT:=de-latin1-nodeadkeys}"
log "Hostname:         ${NEWHOST:?"Set a hostname"}"
log "Packages:         ${PACKAGES:=sudo,dhcpcd,git,neovim,zsh}"

loadkeys "$KEY_LAYOUT"

log
log "Internet"
ip a
ping -c 1 google.com

log
log "Setup NTP..."
timedatectl set-ntp true

log
log "Run fdisk, mkfs.<fs>, mkswap, mount /dev/<root part> /mnt, swapon /dev/<swap part>"
log "Then press C-D or type exit to continue installation."
lsblk
log
bash

log
log "Update mirrorlist..."
reflector --protocol https \
          --country "$COUNTRIES" \
          --sort     rate \
          --latest   20 \
          --age      12 \
          | tee /etc/pacman.d/mirrorlist

log
log "Check for NVIDIA graphics card..."
if lspci -k | grep -i "NVIDIA" > /dev/null; then
    log "found, added nvidia to \$PACKAGES"
    PACKAGES="$PACKAGES,nvidia-dkms"
else
    log "not found"
fi

log
log "Install packages on new system..."
# shellcheck disable=SC2046
pacstrap /mnt base linux{,-headers} linux-firmware $(printf "%s" "$PACKAGES" | tr ',' ' ')

log
log "Generate fstab..."
genfstab -U /mnt >> /mnt/etc/fstab

log
log "Chroot into new root"

cat << EOR > install-rooted.sh
#!/bin/bash
set -euo pipefail

log() {
    printf "\\033[32m%s\\033[39m\\n" "\$*"
}

log
log "Configure timezone..."
ln -sf "/usr/share/zoneinfo/$TIMEZONE" /etc/localtime
timedatectl set-ntp true
hwclock --systohc

log
log "Generate locales..."
echo  "$LOCALES" | tr ',' '\\n' | sed 's/$/.UTF-8 UTF-8/g' >> /etc/locale.gen
locale-gen

log
log "Set locales..."
echo "$LOCALES" | tr ',' '\\n' | head -n1 | sed 's/$/.UTF-8 UTF-8/g' > /etc/locale.conf

log
log "Keyboard layout..."
if [[ -e /etc/vconsole.conf ]]; then
    sed -i "s/KEYMAP=.*$/KEYMAP=$KEY_LAYOUT/g" -i /etc/vconsole.conf
else
    echo "KEYMAP=$KEY_LAYOUT" > /etc/vconsole.conf
fi

log
log "Set hostname..."
echo "$NEWHOST" > /etc/hostname

log
log "Set hosts..."
cat << EOH > /etc/hots
127.0.0.1	localhost
::1		    localhost
127.0.1.1	$NEWHOST
EOH

log
log "Type new root password:"
passwd

log
log "Now you can install a boot loader:"
log "GRUB:         https://wiki.archlinux.org/title/GRUB#Installation_2"
log "systemd-boot: https://wiki.archlinux.org/title/Systemd-boot#Installation"
log
log "After rebooting download and run ./install-cli.sh or ./install-awesome.sh"

bash
EOR

chmod +x install-rooted.sh
mount --bind . /mnt/mnt
arch-chroot /mnt /mnt/install-rooted.sh

